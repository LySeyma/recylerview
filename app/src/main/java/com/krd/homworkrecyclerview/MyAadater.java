package com.krd.homworkrecyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAadater extends RecyclerView.Adapter <MyAadater.ViewHolder> {

 Card[] cards;
    Context context;
    public MyAadater (Context context, Card[] cards ) {
        this.context = context;
        this.cards = cards;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItems= layoutInflater.inflate(R.layout.activity_list_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItems);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Card card = cards[position];

    }

    @Override
    public int getItemCount() {
        return cards.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title,txt_description;
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_title = itemView.findViewById(R.id.txt_title);
            this.txt_description = itemView.findViewById(R.id.txt_description);
            this.imageView = itemView.findViewById(R.id.image_view);
        }
    }
}


