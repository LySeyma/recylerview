package com.krd.homworkrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList title = new ArrayList<>(Arrays.asList("Person 1", "Person 2", "Person 3", "Person 4", "Person 5", "Person 6", "Person 7","Person 8", "Person 9", "Person 10", "Person 11", "Person 12", "Person 13", "Person 14"));
    ArrayList description = new ArrayList<>(Arrays.asList("Person 1", "Person 2", "Person 3", "Person 4", "Person 5", "Person 6", "Person 7","Person 8", "Person 9", "Person 10", "Person 11", "Person 12", "Person 13", "Person 14"));
    ArrayList image = new ArrayList<>(Arrays.asList(R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images,R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images, R.drawable.images));
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        Card[] card = new Card[]{
                new Card(1,"Nara","Student at HRD"),
                new Card(1,"Rena","Student at RUPP"),
                new Card(1,"Chan","Student at UC"),
                new Card(1,"Ranta","Student at UP "),
                new Card(1,"Kaka","Student at UC"),
                new Card(1,"Lyna","Student at PPIU"),
                new Card(1,"Mara","Student at RUPP"),
                new Card(1,"Koko","Student at MU"),
                new Card(1,"Reo","Student at CU"),
                new Card(1,"Sana","Student at BBU")
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        MyAadater myAadater = new MyAadater(this,card);
        recyclerView.setAdapter(myAadater);


    }

}